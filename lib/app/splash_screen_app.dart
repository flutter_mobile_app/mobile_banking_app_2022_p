import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:mobile_banking_app/screens/home_screen.dart';
import 'package:mobile_banking_app/screens/login_screen.dart';
import 'package:splashscreen/splashscreen.dart';

class SplashScreenApp extends StatefulWidget {
  const SplashScreenApp({Key? key}) : super(key: key);

  @override
  State<SplashScreenApp> createState() => _SplashScreenAppState();
}

class _SplashScreenAppState extends State<SplashScreenApp> {
  final LocalStorage storage = new LocalStorage('mobile_token_app');
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 1,
      navigateAfterSeconds: FutureBuilder(
        future: storage.ready,
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if(snapshot.data == null){
            return  Center(
              child: CircularProgressIndicator()
            );
          }
          var user = storage.getItem("data");
          if(user != null){
            return HomeScreen();
          }
          return LoginScreen();
        },
      ),
      loaderColor: Colors.black54,
      image: Image.asset("assets/images/logo.png"),
      photoSize: 100,
      backgroundColor: Colors.blueGrey,
    );
  }
}
