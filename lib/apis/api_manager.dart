import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:mobile_banking_app/apis/apis.dart';
import 'package:mobile_banking_app/models/category/Category.dart';
import 'package:mobile_banking_app/models/category/CategoryByIdRes.dart';
import 'package:mobile_banking_app/models/category/CategoryCreateRes.dart';
import 'package:mobile_banking_app/models/category/CategoryRes.dart';
import 'package:mobile_banking_app/models/register/RegisterReq.dart';
import 'package:mobile_banking_app/models/register/RegisterRes.dart';
import 'package:mobile_banking_app/models/req/login_req.dart';
import 'package:mobile_banking_app/models/res/login_res.dart';
import 'package:mobile_banking_app/models/user/UserReq.dart';
import 'package:mobile_banking_app/models/user/UserRes.dart';

class ApiManager extends Apis {
  Future<LoginRes> login(LoginReq req) async {
    LoginRes res = LoginRes();
    try {
      var url = Uri.parse(baseUrl + loginUrl);
      var response = await http.post(url,
          headers: headers,
          body: jsonEncode(req.toJson()),
          encoding: Encoding.getByName("utf-8"));
      if (response.statusCode == 401) {
        throw ("Your phone number and password incorrect!");
      }
      if (response.body.isNotEmpty) {
        Map map = json.decode(response.body);
        res = LoginRes.formJson(map);
      }
    } catch (e) {
      print("ERROR ${e}");
      throw ("Internal server error");
    }
    return res;
  }

  Future<UserRes> getUserInfo() async {
    UserRes res = UserRes();
    UserReq req = UserReq();
    try {
      req.userAccountId = "";
      req.deviceId = "";
      var url = Uri.parse(baseUrl + getUserInfoUrl);
      var response = await http.post(url,
          headers: headerWithToken(getToken()),
          body: jsonEncode(req.toJson()),
          encoding: Encoding.getByName("uft8"));
      if (response.statusCode != 200) {
        throw ("Data not found");
      }
      if (response.statusCode == 200) {
        Map map = jsonDecode(response.body);
        res = UserRes.fromJson(map);
      }
    } catch (error) {
      print("${error.toString()}");
      throw ("${error}");
    }
    return res;
  }

  Future<RegisterRes> openAccount(RegisterReq req) async {
    try {
      RegisterRes res = RegisterRes();
      var url = Uri.parse(baseUrl + openAccountUrl);
      var response = await http.post(url,
          headers: headers, body: jsonEncode(req.toJson()));
      if (response.statusCode == 200 ||
          response.statusCode == 400 ||
          response.statusCode == 500) {
        final bodyResponse = jsonDecode(response.body);
        res = RegisterRes.fromJson(bodyResponse);
        if (res.code == "400") {
          throw ("${res.data}");
        }
        if (res.code == "500") {
          throw ("${res.message}");
        }
      }
      return res;
    } catch (e) {
      throw ("General Error");
    }
  }

  Future<CategoryRes> getAllCategory() async {
    CategoryRes res = CategoryRes();
    try {
      var url = Uri.parse(baseUrl + getCategoryUrl);
      var response = await http.get(
        url,
        headers: headerWithToken(getToken()),
      );
      if (response.statusCode == 200) {
        final responseMap = jsonDecode(utf8.decode(response.bodyBytes));
        res = CategoryRes.fromJson(responseMap);
      }
    } catch (e) {
      throw ("General Error");
    }
    return res;
  }

  Future<CategoryCreateRes> createCategory(Category req) async {
    CategoryCreateRes res = CategoryCreateRes();
    try {
      var url = Uri.parse(baseUrl + createCategoryUrl);
      var response = await http.post(url,
          headers: headerWithToken(getToken()), body: jsonEncode(req.toJson()));
      if (response.statusCode == 200) {
        final map = jsonDecode(response.body);
        res = CategoryCreateRes.fromJson(map);
      }
    } catch (e) {
      throw ("${e}");
    }
    return res;
  }

  Future<CategoryCreateRes> updateCategory(Category req) async {
    CategoryCreateRes res = CategoryCreateRes();
    try {
      var url = Uri.parse(baseUrl + updateCategoryUrl);
      var response = await http.post(url,
          headers: headerWithToken(getToken()), body: jsonEncode(req.toJson()));
      if (response.statusCode == 200) {
        final map = jsonDecode(response.body);
        res = CategoryCreateRes.fromJson(map);
      }
    } catch (e) {
      throw ("${e}");
    }
    return res;
  }

  Future<CategoryCreateRes> deleteCategory(Category req) async {
    CategoryCreateRes res = CategoryCreateRes();
    try {
      var url = Uri.parse(baseUrl + deleteCategoryUrl);
      var response = await http.post(url,
          headers: headerWithToken(getToken()), body: jsonEncode(req.toJson()));
      if (response.statusCode == 200) {
        final map = jsonDecode(response.body);
        res = CategoryCreateRes.fromJson(map);
      }
    } catch (e) {
      throw ("${e}");
    }
    return res;
  }

  Future<CategoryByIdRes> getCategoryById(int categoryId) async {
    CategoryByIdRes res = CategoryByIdRes();
    try {
      var url = Uri.parse(baseUrl + getCategoryByIdUrl + categoryId.toString());
      var response = await http.get(
        url,
        headers: headerWithToken(getToken()),
      );
      if (response.statusCode == 200) {
        final responseMap = jsonDecode(utf8.decode(response.bodyBytes));
        res = CategoryByIdRes.fromJson(responseMap);
      }
    } catch (e) {
      throw ("General Error");
    }
    return res;
  }

  String getToken() {
    LoginRes res = LoginRes();
    final storage = LocalStorage("mobile_token_app");
    if (null != storage.getItem("data")) {
      res = LoginRes.formJson(storage.getItem("data"));
    }
    return res.accessToken!;
  }
}
