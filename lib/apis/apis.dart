abstract class Apis {
  String baseUrl = "http://143.198.88.147:20022";
  String loginUrl = "/api/oauth/token";
  String getUserInfoUrl = "/api/app/user/info";
  String openAccountUrl = "/api/oauth/open/account";
  String getCategoryUrl = "/api/app/category/list";
  String createCategoryUrl = "/api/app/category/create";
  String updateCategoryUrl = "/api/app/category/update";
  String deleteCategoryUrl = "/api/app/category/delete";
  String getCategoryByIdUrl = "/api/app/category/";

  Map<String, String> headers = {"Content-Type": "application/json"};

  Map<String, String> headerWithToken(String token) {
    return {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
  }

}
