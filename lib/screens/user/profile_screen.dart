import 'package:flutter/material.dart';
import 'package:mobile_banking_app/apis/api_manager.dart';
import 'package:mobile_banking_app/models/user/User.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool _isLoading = false;
  User user = User();
  @override
  void initState() {
    getUserInfo();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User Profile"),
      ),
      body: _isLoading == true
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height: 250,
                      decoration: BoxDecoration(
                        color: Colors.blueGrey,
                      ),
                      child: Center(
                        child: Container(
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(50)),
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text("${user.firstName ?? ""}"),
                      subtitle: Text("First Name"),
                    ),
                    Divider(
                      height: 1,
                    ),
                    ListTile(
                      title: Text("${user.lastName}"),
                      subtitle: Text("Last Name"),
                    ),
                    ListTile(
                      title: Text("${user.username}"),
                      subtitle: Text("Username"),
                    ),
                    ListTile(
                      title: Text("${user.phoneNumber}"),
                      subtitle: Text("Phone Number"),
                    ),
                    ListTile(
                      title: Text("${user.email}"),
                      subtitle: Text("Email"),
                    ),
                    ListTile(
                      title: Text("${user.dob}"),
                      subtitle: Text("Date Of Birt"),
                    ),
                    ListTile(
                      title: Text("${user.nationalId}"),
                      subtitle: Text("National ID"),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  getUserInfo() {
    setState(() {
      _isLoading = true;
    });
    ApiManager().getUserInfo().then((value) {
      setState(() {
        _isLoading = false;
        user = value.data!.user!;
      });
    }).catchError((onError) {
      final snackBar = SnackBar(
        content: Text('${onError}'),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      setState(() {
        _isLoading = false;
      });
    });
  }
}
