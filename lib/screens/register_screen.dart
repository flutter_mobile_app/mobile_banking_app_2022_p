import 'package:flutter/material.dart';
import 'package:mobile_banking_app/apis/api_manager.dart';
import 'package:mobile_banking_app/models/register/RegisterReq.dart';
import 'package:mobile_banking_app/widgets/button_custom_widget.dart';
import 'package:mobile_banking_app/widgets/input_text_widget.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final _usernameController =  TextEditingController();
  final _firstNameController =  TextEditingController();
  final _lastController =  TextEditingController();
  final _phoneController =  TextEditingController();
  final _emailController =  TextEditingController();
  final _dobController =  TextEditingController();
  final _nationalIdController =  TextEditingController();
  final _passwordController =  TextEditingController();
  final _confirmController =  TextEditingController();

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register Screen"),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            child: Column(
              children: [
                InputTextWidget(
                  label:"First Name",
                  controller: _firstNameController,
                ),
                InputTextWidget(
                  label:"Last Name",
                  controller: _lastController,
                ),
                InputTextWidget(
                  label:"User Name",
                  controller: _usernameController,
                ),
                InputTextWidget(
                  label:"Phone",
                  controller: _phoneController,
                ),
                InputTextWidget(
                  label:"Email",
                  controller: _emailController,
                ),
                InputTextWidget(
                  label:"Date of Birth",
                  controller: _dobController,
                ),
                InputTextWidget(
                  label:"National Id",
                  controller: _nationalIdController,
                ),
                InputTextWidget(
                  label:"Password",
                  controller: _passwordController,
                ),
                InputTextWidget(
                  label:"Confirm Password",
                  controller: _confirmController,
                ),
                ButtonCustomWidget(
                  name: "Open Account",
                  onClick: (){
                    openAccount();
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  openAccount(){
    if(_formKey.currentState!.validate()){
      RegisterReq req =  RegisterReq();
      req.firstName=_firstNameController.text;
      req.lastName=_lastController.text;
      req.username=_usernameController.text;
      req.phoneNumber=_phoneController.text;
      req.email=_emailController.text;
      req.dob=_dobController.text;
      req.password=_passwordController.text;
      req.confirmPassword=_confirmController.text;
      req.nationalId=_nationalIdController.text;
      ApiManager().openAccount(req).then((value){
        setState(() {
          _isLoading = false;
          final snackBar = SnackBar(
            content:  Text('${value.data}'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          Navigator.of(context).pushNamedAndRemoveUntil("/", (route) => false);
        });
      }).catchError((e){
        final snackBar = SnackBar(
          content:  Text('${e}'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        setState(() {
          _isLoading = false;
        });
      });
    }
  }
}
