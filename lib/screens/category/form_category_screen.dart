import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:mobile_banking_app/apis/api_manager.dart';
import 'package:mobile_banking_app/models/category/Category.dart';
import 'package:mobile_banking_app/widgets/button_custom_widget.dart';
import 'package:mobile_banking_app/widgets/button_loading_custom_widget.dart';
import 'package:mobile_banking_app/widgets/input_text_widget.dart';

class FormCategoryScreen extends StatefulWidget {
  Category? category;

  FormCategoryScreen({Key? key, this.category}) : super(key: key);

  @override
  State<FormCategoryScreen> createState() => _FormCategoryScreenState();
}

class _FormCategoryScreenState extends State<FormCategoryScreen> {
  var _nameController = TextEditingController();
  var _nameKhController = TextEditingController();
  var _routeController = TextEditingController();
  var _categoryCodeController = TextEditingController();
  var _indexController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  void initState() {
    if (null != widget.category) {
      getCategoryById(widget.category!.id!);
    }
    super.initState();
  }

  getCategoryById(id) {
    setState(() {
      _isLoading == true;
    });
    ApiManager().getCategoryById(id).then((value) {
      if (value.data == null) {
        Navigator.pop(context, true);
      }
      var object = value.data;
      _nameController.text = object!.name!;
      _nameKhController.text = object.nameKh!;
      _routeController.text = object.route!;
      _indexController.text = object.index!.toString();
      _categoryCodeController.text = object.categoryCode!;
      setState(() {
        _isLoading == false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.category == null
            ? "create_category".tr()
            : "update_category".tr()),
      ),
      body: SingleChildScrollView(
        child: _isLoading == true
            ? Center(child: CircularProgressIndicator())
            : Container(
                padding: EdgeInsets.all(20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      InputTextWidget(
                        controller: _nameController,
                        icon: Icon(Icons.edit),
                        label: "name".tr(),
                      ),
                      InputTextWidget(
                        controller: _nameKhController,
                        icon: Icon(Icons.edit),
                        label: "name_kh".tr(),
                      ),
                      InputTextWidget(
                        controller: _routeController,
                        icon: Icon(Icons.edit),
                        label: "route".tr(),
                      ),
                      InputTextWidget(
                        controller: _categoryCodeController,
                        icon: Icon(Icons.edit),
                        label: "category_code".tr(),
                      ),
                      InputTextWidget(
                        controller: _indexController,
                        icon: Icon(Icons.edit),
                        label: "index".tr(),
                      ),
                      _isLoading == true
                          ? ButtonLoadingCustomWidget()
                          : ButtonCustomWidget(
                              name: widget.category == null
                                  ? "create".tr()
                                  : "update".tr(),
                              onClick: () {
                                if (widget.category == null) {
                                  create();
                                } else {
                                  update();
                                }
                              },
                            ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  create() {
    if (_formKey.currentState!.validate()) {
      Category req = Category();
      req.id = 0;
      req.name = _nameController.text;
      req.nameKh = _nameKhController.text;
      req.route = _routeController.text;
      req.categoryCode = _categoryCodeController.text;
      req.imageUrl = null;
      req.status = "ACT";
      req.index = int.parse(_indexController.text);
      ApiManager().createCategory(req).then((value) {
        final snackBar = SnackBar(
          content: Text("${value.message!}"),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.pop(context, true);
      }).catchError((onError) {
        final snackBar = SnackBar(
          content: const Text('Can not create category'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        setState(() {
          this._isLoading = false;
        });
      });
    }
  }

  update() {
    if (_formKey.currentState!.validate()) {
      Category req = Category();
      req.id = widget.category!.id!;
      req.name = _nameController.text;
      req.nameKh = _nameKhController.text;
      req.route = _routeController.text;
      req.categoryCode = _categoryCodeController.text;
      req.imageUrl = null;
      req.status = "ACT";
      req.index = int.parse(_indexController.text);
      ApiManager().updateCategory(req).then((value) {
        final snackBar = SnackBar(
          content: Text("${value.message!}"),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        Navigator.pop(context, true);
      }).catchError((onError) {
        final snackBar = SnackBar(
          content: const Text('Can not update category'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        setState(() {
          this._isLoading = false;
        });
      });
    }
  }
}
