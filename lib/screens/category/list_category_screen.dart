import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:mobile_banking_app/apis/api_manager.dart';
import 'package:mobile_banking_app/models/category/Category.dart';
import 'package:mobile_banking_app/screens/category/form_category_screen.dart';

class ListCategoryScreen extends StatefulWidget {
  const ListCategoryScreen({Key? key}) : super(key: key);

  @override
  State<ListCategoryScreen> createState() => _ListCategoryScreenState();
}

class _ListCategoryScreenState extends State<ListCategoryScreen> {
  List<Category> categoryList = [];
  bool _isLoading = false;
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    getAllCategory();
    super.initState();
  }

  getAllCategory() {
    setState(() {
      _isLoading = true;
    });
    ApiManager().getAllCategory().then((value) {
      setState(() {
        categoryList = value.data!.menuList!;
        _isLoading = false;
      });
    });
  }

  Future<void> _onRefresh() async {
    getAllCategory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("list_category".tr()),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FormCategoryScreen()))
                    .then((value) {
                  if (value == true) {
                    getAllCategory();
                  }
                });
              },
              icon: Icon(Icons.add))
        ],
      ),
      body: _isLoading == true
          ? Center(child: CircularProgressIndicator())
          : RefreshIndicator(
              onRefresh: _onRefresh,
              key: _refreshIndicatorKey,
              child: Container(
                padding: EdgeInsets.all(10),
                child: ListView.builder(
                    itemCount: categoryList.length,
                    itemBuilder: (BuildContext context, index) {
                      var category = categoryList[index];
                      return Card(
                        child: ListTile(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormCategoryScreen(
                                          category: category,
                                        ))).then((value) {
                              if (value == true) {
                                getAllCategory();
                              }
                            });
                          },
                          trailing: IconButton(
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext ctx) {
                                    return AlertDialog(
                                      title: Text('Confirm Delete'.tr()),
                                      content: Text(
                                          "Do you want to delete category: ${category.name!}"),
                                      actions: [
                                        // The "Yes" button
                                        TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text('No'.tr())),
                                        TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                              deleteCategory(category);
                                            },
                                            child: Text('Yes'.tr()))
                                      ],
                                    );
                                  });
                            },
                            icon: Icon(
                              Icons.delete,
                              color: Colors.redAccent,
                            ),
                          ),
                          title: Text("${category.name}"),
                          subtitle: Text("${category.nameKh}"),
                        ),
                      );
                    }),
              ),
            ),
    );
  }

  deleteCategory(Category req) {
    ApiManager().deleteCategory(req).then((value) {
      print("${value.message!}");
      getAllCategory();
    });
  }
}
