import 'package:flutter/material.dart';
import 'package:mobile_banking_app/widgets/input_text_widget.dart';

class OpenAccountScreen extends StatefulWidget {
  const OpenAccountScreen({Key? key}) : super(key: key);

  @override
  State<OpenAccountScreen> createState() => _OpenAccountScreenState();
}

class _OpenAccountScreenState extends State<OpenAccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Open Account"),
      ),
      body:  SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              InputTextWidget(
                label: "First Name",
              ),
              InputTextWidget(
                label: "Last Name",
              ),
              InputTextWidget(
                label: "User Name",
              ),
              InputTextWidget(
                label: "Phone Number",
              ),
              InputTextWidget(
                label: "Email",
              ),
              InputTextWidget(
                label: "National ID",
              ),
              InputTextWidget(
                label: "Date Of Birth",
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
