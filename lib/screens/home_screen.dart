import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:mobile_banking_app/apis/api_manager.dart';
import 'package:mobile_banking_app/models/category/Category.dart';
import 'package:mobile_banking_app/screens/category/list_category_screen.dart';
import 'package:mobile_banking_app/screens/change_language_screen.dart';
import 'package:mobile_banking_app/screens/user/profile_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final LocalStorage storage = new LocalStorage('mobile_bankin_app');
  bool isLoading = false;
  List<Category> categories = [];

  @override
  void initState() {
    getCategory();
    super.initState();
  }

  getCategory() {
    setState(() {
      this.isLoading = true;
    });
    ApiManager().getAllCategory().then((value) {
      setState(() {
        this.isLoading = false;
        categories = value.data!.menuList!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: [
            Container(
              color: Colors.blueGrey,
              height: 250,
            ),
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProfileScreen()));
              },
              leading: Icon(Icons.supervised_user_circle_outlined),
              title: Text("Profile"),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ListCategoryScreen()));
              },
              leading: Icon(Icons.list),
              title: Text("Categories"),
            ),
            ListTile(
              title: Text("logout".tr()),
              leading: Icon(Icons.exit_to_app),
              onTap: (){
                showDialog(
                    context: context,
                    builder: (BuildContext ctx) {
                      return AlertDialog(
                        title: Text('please_confirm'.tr()),
                        content: Text('are_you_sure_to_sign_out_the_app'.tr()),
                        actions: [
                          // The "Yes" button
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text('n'.tr())),
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                setState(() {
                                  final localStorage = LocalStorage("mobile_token_app");
                                  final localShop = LocalStorage("data");
                                  localShop.clear();
                                  localStorage.clear();
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      "/", (route) => false);
                                });
                              },
                              child: Text('y'.tr()))
                        ],
                      );
                    });
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Text("Mobile Banking App"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ChangeLanguageScreen(),
                  ),
                ).then((value) {
                  if (value == "KM") {
                    context.setLocale(Locale('km', 'KM'));
                  }
                  if (value == "EN") {
                    context.setLocale(Locale("en", "EN"));
                  }
                });
              },
              icon: Icon(Icons.language)),
        ],
      ),
      // body: Center(
      //   child: TextButton(
      //     onPressed: (){
      //       storage.clear();
      //       Navigator.of(context).pushNamedAndRemoveUntil("/", (route) => false);
      //     },
      //     child: Text("Logout"),
      //   ),
      // ),
      body: Container(
        margin: const EdgeInsets.all(10),
        child: GridView.count(
          crossAxisCount: 3,
          mainAxisSpacing: 3,
          crossAxisSpacing: 3,
          children: List.generate(
            categories.length,
            (index) {
              var category = categories[index];
              return Container(
                decoration: BoxDecoration(
                    color: Colors.blueGrey,
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "english".tr() == "English" ? category.name! : category.nameKh!,
                      style: TextStyle(color: Colors.white, fontSize: 11),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
