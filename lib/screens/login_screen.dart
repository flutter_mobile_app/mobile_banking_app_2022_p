import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:mobile_banking_app/apis/api_manager.dart';
import 'package:mobile_banking_app/models/req/login_req.dart';
import 'package:mobile_banking_app/screens/change_language_screen.dart';
import 'package:mobile_banking_app/widgets/button_custom_widget.dart';
import 'package:mobile_banking_app/widgets/button_loading_custom_widget.dart';
import 'package:mobile_banking_app/widgets/input_text_widget.dart';

import 'register_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      _usernameController.text = "0962505045";
      _passwordController.text = "123456";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("login".tr()),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ChangeLanguageScreen(),
                  ),
                ).then((value) {
                  if (value == "KM") {
                    context.setLocale(Locale('km', 'KM'));
                  }
                  if (value == "EN") {
                    context.setLocale(Locale("en", "EN"));
                  }
                });
              },
              icon: Icon(Icons.language)),
        ],
      ),
      body: Form(
        key: _formKey,
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                    child: Text(
                  "login".tr(),
                  style: TextStyle(
                      color: Colors.blueGrey,
                      fontWeight: FontWeight.bold,
                      fontSize: 24),
                )),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                    child: Text(
                        "You can login your account banking \n with phone number and username")),
              ),
              InputTextWidget(
                controller: _usernameController,
                icon: Icon(Icons.phone),
                label: "Phone Or Account no",
              ),
              InputTextWidget(
                controller: _passwordController,
                icon: Icon(Icons.password),
                password: true,
                label: "password",
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Forget Password?"),
              ),
              _isLoading == true
                  ? ButtonLoadingCustomWidget()
                  : ButtonCustomWidget(
                      name: "Login",
                      onClick: () {
                        login();
                      },
                    ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("No acccount yet?"),
              ),
              ButtonCustomWidget(
                name: "Open Account",
                onClick: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (contex) => RegisterScreen()));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void login() {
    final LocalStorage storage = new LocalStorage('mobile_token_app');
    if (_formKey.currentState!.validate()) {
      var phoneNumber = _usernameController.text;
      var password = _passwordController.text;
      LoginReq req = LoginReq();
      req.phoneNumber = phoneNumber;
      req.password = password;
      setState(() {
        this._isLoading = true;
      });
      ApiManager().login(req).then((value) {
        setState(() {
          this._isLoading = false;
          storage.setItem("data", value.toJson());
          Navigator.of(context).pushNamedAndRemoveUntil("/", (route) => false);
        });
      }).catchError((onError) {
        final snackBar = SnackBar(
          content: const Text('Your username and password incorrect!'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        setState(() {
          this._isLoading = false;
        });
      });
    }
  }
}
