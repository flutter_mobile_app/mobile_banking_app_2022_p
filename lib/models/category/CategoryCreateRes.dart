class CategoryCreateRes {
  CategoryCreateRes({
      this.message, 
      this.code, 
      this.data,});

  CategoryCreateRes.fromJson(dynamic json) {
    message = json['message'];
    code = json['code'];
    data = json['data'];
  }
  String? message;
  String? code;
  String? data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    map['code'] = code;
    map['data'] = data;
    return map;
  }

}