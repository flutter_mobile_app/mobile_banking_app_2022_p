import 'package:mobile_banking_app/models/category/Category.dart';

class Data {
  Data({
      this.menuList,});

  Data.fromJson(dynamic json) {
    if (json['menuList'] != null) {
      menuList = [];
      json['menuList'].forEach((v) {
        menuList!.add(Category.fromJson(v));
      });
    }
  }
  List<Category>? menuList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (menuList != null) {
      map['menuList'] = menuList!.map((v) => v.toJson()).toList();
    }
    return map;
  }

}