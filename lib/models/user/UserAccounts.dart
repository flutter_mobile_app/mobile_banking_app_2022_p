class UserAccounts {
  UserAccounts({
      this.createAt, 
      this.createBy, 
      this.updateAt, 
      this.updateBy, 
      this.id, 
      this.userAccountId, 
      this.accountNumber, 
      this.accountCcy, 
      this.totalBalance, 
      this.status, 
      this.accountDefault, 
      this.accountType,});

  UserAccounts.fromJson(Map<dynamic, dynamic> json) {
    createAt = json['createAt'];
    createBy = json['createBy'];
    updateAt = json['updateAt'];
    updateBy = json['updateBy'];
    id = json['id'];
    userAccountId = json['userAccountId'];
    accountNumber = json['accountNumber'];
    accountCcy = json['accountCcy'];
    totalBalance = json['totalBalance'];
    status = json['status'];
    accountDefault = json['accountDefault'];
    accountType = json['accountType'];
  }
  String? createAt;
  String? createBy;
  String? updateAt;
  String? updateBy;
  int? id;
  String? userAccountId;
  String? accountNumber;
  String? accountCcy;
  double? totalBalance;
  String? status;
  String? accountDefault;
  String? accountType;

  Map<dynamic, dynamic> toJson() {
    final map = <dynamic, dynamic>{};
    map['createAt'] = createAt;
    map['createBy'] = createBy;
    map['updateAt'] = updateAt;
    map['updateBy'] = updateBy;
    map['id'] = id;
    map['userAccountId'] = userAccountId;
    map['accountNumber'] = accountNumber;
    map['accountCcy'] = accountCcy;
    map['totalBalance'] = totalBalance;
    map['status'] = status;
    map['accountDefault'] = accountDefault;
    map['accountType'] = accountType;
    return map;
  }

}