import 'Roles.dart';

class User {
  User({
      this.id, 
      this.username, 
      this.firstName, 
      this.lastName, 
      this.email, 
      this.phoneNumber, 
      this.password, 
      this.nationalId, 
      this.dob, 
      this.status, 
      this.userAccountId, 
      this.roles,});

  User.fromJson(dynamic json) {
    id = json['id'];
    username = json['username'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    email = json['email'];
    phoneNumber = json['phoneNumber'];
    password = json['password'];
    nationalId = json['nationalId'];
    dob = json['dob'];
    status = json['status'];
    userAccountId = json['userAccountId'];
    if (json['roles'] != null) {
      roles = [];
      json['roles'].forEach((v) {
        roles!.add(Roles.fromJson(v));
      });
    }
  }
  int? id;
  String? username;
  String? firstName;
  String? lastName;
  String? email;
  String? phoneNumber;
  String? password;
  String? nationalId;
  String? dob;
  String? status;
  String? userAccountId;
  List<Roles>? roles;

  Map<dynamic, dynamic> toJson() {
    final map = <dynamic, dynamic>{};
    map['id'] = id;
    map['username'] = username;
    map['firstName'] = firstName;
    map['lastName'] = lastName;
    map['email'] = email;
    map['phoneNumber'] = phoneNumber;
    map['password'] = password;
    map['nationalId'] = nationalId;
    map['dob'] = dob;
    map['status'] = status;
    map['userAccountId'] = userAccountId;
    if (roles != null) {
      map['roles'] = roles!.map((v) => v.toJson()).toList();
    }
    return map;
  }

}