import 'User.dart';
import 'UserAccounts.dart';

class Data {
  Data({
      this.user, 
      this.userAccounts,});

  Data.fromJson(Map<dynamic, dynamic> json) {
    user = json['user'] != null ? User.fromJson(json['user']) : null;
    if (json['userAccounts'] != null) {
      userAccounts = [];
      json['userAccounts'].forEach((v) {
        userAccounts!.add(UserAccounts.fromJson(v));
      });
    }
  }
  User? user;
  List<UserAccounts>? userAccounts;

  Map<dynamic, dynamic> toJson() {
    final map = <dynamic, dynamic>{};
    if (user != null) {
      map['user'] = user!.toJson();
    }
    if (userAccounts != null) {
      map['userAccounts'] = userAccounts!.map((v) => v.toJson()).toList();
    }
    return map;
  }

}