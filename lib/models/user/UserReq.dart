class UserReq {
  UserReq({
      this.userAccountId, 
      this.deviceId,});

  UserReq.fromJson(Map<dynamic, dynamic> json) {
    userAccountId = json['userAccountId'];
    deviceId = json['deviceId'];
  }
  String? userAccountId;
  String? deviceId;

  Map<dynamic, dynamic> toJson() {
    final map = <dynamic, dynamic>{};
    map['userAccountId'] = userAccountId;
    map['deviceId'] = deviceId;
    return map;
  }

}