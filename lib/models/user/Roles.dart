class Roles {
  Roles({
      this.id, 
      this.name,});

  Roles.fromJson(Map<dynamic, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }
  int? id;
  String? name;

  Map<dynamic, dynamic> toJson() {
    final map = <dynamic, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    return map;
  }

}